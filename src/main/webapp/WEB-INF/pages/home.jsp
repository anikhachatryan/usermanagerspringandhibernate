<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ani
  Date: 31.07.19
  Time: 17:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<div align="center">
    <h1>User List</h1>

    <table border="1">

        <th>Name</th>
        <th>Surname</th>
        <th>Email</th>

        <c:forEach var="user" items="${users}">
            <tr>

                <td>${user.name}</td>
                <td>${user.surname}</td>
                <td>${user.email}</td>
                <td><a href="/editUser?id=${user.id}">Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp; <a
                            href="/deleteUser?id=${user.id}">Delete</a></td>

            </tr>
        </c:forEach>
    </table>
    <h4>
        New User Register <a href="/newUser">here</a>
    </h4>
</div>
</body>
</html>
