package am.egs.dao;

import am.egs.model.User;

import java.util.List;

public interface UserDAO {

    void add(User user);

    void delete(int id);

    List<User> getAll();

    User update(User user);

    User get(int id);
}
